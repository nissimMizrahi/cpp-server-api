#include "WsaThing.h"

WsaThing::WsaThing()
{
	auto iResult = WSAStartup(MAKEWORD(2, 0), &wsaData);;
	if (iResult != 0) 
	{
		throw std::exception("WSAStartup failed with error: ");
	}
}

WsaThing::~WsaThing()
{
	WSACleanup();
}
