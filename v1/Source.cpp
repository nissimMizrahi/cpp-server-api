#include <iostream>
#include <string_view>

#include "TcpServer.h"

class myServer : public TcpServer
{

public:
	myServer(std::string ip, int port) : TcpServer(ip, port) {
	}

	void handleClient(std::shared_ptr<TcpClient> client) override
	{
		std::cout << "client\n";

		while (client->is_alive())
		{
			std::cout << "alive\n";
			std::vector<char> buff;
			client->getSock().recv_all(buff);

			if (buff.size())
			{
				std::string_view s(buff.data(), buff.size());
				std::cout << s << std::endl;
			}
			
		}
		std::cout << "closed\n";
	}
};

int main()
{
	myServer("0.0.0.0", 5555).run();
	return 0;
}