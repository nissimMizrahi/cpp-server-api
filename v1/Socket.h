#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#pragma comment (lib, "Ws2_32.lib")

class Socket
{
public:
	Socket();

	virtual void send(char*, size_t) = 0;
	virtual size_t recv(char*, size_t) = 0;

	virtual bool is_alive() = 0;

	virtual ~Socket() = 0;
};

