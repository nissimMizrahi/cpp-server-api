#pragma once

#include <vector>
#include "Socket.h"
class TcpSocket : public Socket
{
	SOCKET m_sock;

public:

	TcpSocket(SOCKET sock);
	
	void send(char* buff, size_t len);
	size_t recv(char* buff, size_t len);
	void recv_all(std::vector<char>& buffer);

	void dontblock();
	void block();
	bool is_alive();

	inline bool operator==(const TcpSocket& other) { return m_sock == other.m_sock; }

	~TcpSocket();
};

