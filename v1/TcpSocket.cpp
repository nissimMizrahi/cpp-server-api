#include "TcpSocket.h"

#define MIN(x, y) (x > y ? y : x)
#define MAX(x, y) (x > y ? x : y)

TcpSocket::TcpSocket(SOCKET sock) : Socket(), m_sock(sock)
{
}

void TcpSocket::send(char* buff, size_t len)
{
	::send(m_sock, buff, len, NULL);
}

size_t TcpSocket::recv(char* buff, size_t len)
{
	return ::recv(m_sock, buff, len, NULL);
}

void TcpSocket::recv_all(std::vector<char>& buffer)
{
	dontblock();
	char buff[1024] = { 0 };

	int rec = 0;
	while ((rec = this->recv(buff, 1024)) > 0)
	{
		buffer.insert(buffer.end(), buff, buff + MAX(rec, 0));
	}
	block();
}

void TcpSocket::dontblock()
{
	u_long mode = 1;  // 1 to enable non-blocking socket
	ioctlsocket(m_sock, FIONBIO, &mode);
}

void TcpSocket::block()
{
	u_long mode = 0;  // 1 to enable non-blocking socket
	ioctlsocket(m_sock, FIONBIO, &mode);
}

bool TcpSocket::is_alive()
{
	dontblock();
	int tmp = 0;
	auto ret = ::recv(m_sock, (char*)& tmp, sizeof(tmp), MSG_PEEK);
	int check = WSAGetLastError();
	block();
	return check == 10035 || ret > 0;
	
}

TcpSocket::~TcpSocket()
{
	closesocket(m_sock);
}
