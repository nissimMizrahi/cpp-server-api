#pragma once
#include <mutex>
#include <memory>

#include "SharedResource.h"

template<class T>
class SharedResource;

template<class T>
class SharedResourceGuard
{
	SharedResource<T> mutex;
public:
	SharedResourceGuard(SharedResource<T>& mutex);
	~SharedResourceGuard();

	T& operator*();
	T* operator->();
};

template<class T>
inline SharedResourceGuard<T>::SharedResourceGuard(SharedResource<T>& mutex) : mutex(mutex)
{
	mutex.mutex_pointer->lock();
}

template<class T>
inline SharedResourceGuard<T>::~SharedResourceGuard()
{
	mutex.mutex_pointer->unlock();
}

template<class T>
inline T& SharedResourceGuard<T>::operator*()
{
	return *mutex.value;
}
template<class T>
inline T* SharedResourceGuard<T>::operator->()
{
	return mutex.value.operator->();
}
