#include "TcpServer.h"
#include <algorithm>
#include <functional>

TcpServer::TcpServer(const std::string& ip, unsigned short port) : wsa(new WsaThing()), clients(std::vector<std::shared_ptr<TcpClient>>())
{
	listener = socket(AF_INET, SOCK_STREAM, 0);

	SOCKADDR_IN serverAddr;

	serverAddr.sin_addr.s_addr = inet_addr(ip.c_str());
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);

	if (bind(listener, (SOCKADDR*)& serverAddr, sizeof(serverAddr)))
	{
		throw std::exception("cant bind server");
	}
}

void TcpServer::run()
{
	::listen(listener, 0);

	while (true)
	{
		SOCKET clientSock;
		SOCKADDR_IN clientAddr;
		int clientAddrSize = sizeof(clientAddr);

		if ((clientSock = accept(listener, (SOCKADDR*)& clientAddr, &clientAddrSize)) != INVALID_SOCKET)
		{
			auto client = std::make_shared<TcpClient>(clientSock);
			clients.lock()->emplace_back(client);
			auto t = std::thread([this, &client]()
				{
					this->handleClient(client);
					killClient(client);
				});

			t.detach();
		}
	}
	
	
}

void TcpServer::handleClient(std::shared_ptr<TcpClient> client)
{
}

void TcpServer::killClient(const std::shared_ptr<TcpClient>& client)
{
	auto clients_vec = clients.lock();
	clients_vec->erase(std::remove_if(clients_vec->begin(), clients_vec->end(), [client](const std::shared_ptr<TcpClient>& c) {
		return c->getSock() == client->getSock();
	}), clients_vec->end());
}

TcpServer::~TcpServer()
{
	listeningThread.join();
	closesocket(listener);
	delete wsa;
}
