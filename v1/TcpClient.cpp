#include "TcpClient.h"
#include <iostream>

TcpClient::TcpClient(SOCKET sock) : sock(sock)
{
	struct sockaddr_in sin;
	socklen_t len = sizeof(sin);
	if (getsockname(sock, (struct sockaddr*) & sin, &len) != -1)
	{
		port = ntohs(sin.sin_port);
		ip = inet_ntoa(sin.sin_addr);
	}
	else
	{
		throw std::exception("cant get info about socket");
	}
}

std::string TcpClient::getIp()
{
	return ip;
}

unsigned short TcpClient::getPort()
{
	return port;
}

TcpSocket& TcpClient::getSock()
{
	return sock;
}

TcpClient::~TcpClient()
{
}
