#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <string>
#include <vector>
#include <memory>
#include <thread>
#include <functional>

#include "SharedResource.h"
#include "TcpClient.h"
#include "WsaThing.h"

class TcpServer
{
	SOCKET listener;
	WsaThing* wsa;

	std::thread listeningThread;
	SharedResource<std::vector<std::shared_ptr<TcpClient>>> clients;
	
	

public:

	TcpServer(const std::string& ip, unsigned short port);
	void run();

	virtual void handleClient(std::shared_ptr<TcpClient> client);
	void killClient(const std::shared_ptr<TcpClient>& client);

	virtual ~TcpServer();
};

