#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <string>
#include "TcpSocket.h"

class TcpClient
{
	std::string ip;
	unsigned short port;

	TcpSocket sock;

public:

	TcpClient(SOCKET sock);

	std::string getIp();
	unsigned short getPort();
	inline bool is_alive() { return sock.is_alive(); }

	TcpSocket& getSock();

	~TcpClient();
};

